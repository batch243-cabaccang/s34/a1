require("dotenv").config();

const express = require("express");
const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// GET
app.get("/home", (req, res) => {
  res.send("This is the Home Page");
});

const usersRoutes = require("./routes/users.js");
app.use("/users", usersRoutes);

// Salpak na lang po kayo ng sariling port nyo. Hehe
app.listen(port, () => {
  console.log(`Listening to Port ${port}`);
});
