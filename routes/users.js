const express = require("express");
const userRoutes = express.Router();
const users = [
  {
    firstName: "Horhe",
    age: 70,
  },
  {
    firstName: "George",
    age: 70,
  },
];

// GET users
userRoutes.get("/", (req, res) => {
  res.json(users);
});

// POST NOT NEEDED BUT STILL. HEHE
// userRoutes.post("/", (req, res) => {
//   const newUser = {
//     firstName: req.body.firstName,
//     age: req.body.age,
//   };
//   if (!req.body.firstName || !req.body.age) {
//     return res.send("Input details");
//   }
//   users.push(newUser);
//   res.json(users);
// });

// DELETE User
userRoutes.delete("/delete-user", (req, res) => {
  for (i = 0; i < users.length; i++) {
    if (req.body.firstName == users[i].firstName) {
      users.splice(i, 1);
      break;
    } else {
      res.send("User not found");
    }
  }
  res.json(users);
});

module.exports = userRoutes;
